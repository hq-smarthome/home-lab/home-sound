#!/usr/bin/env sh

set -o nounset

echo "starting pulseaudio"
set -x

exec sg pulseaudio -c "pulseaudio --file /etc/pulse/home-sound.pa"
