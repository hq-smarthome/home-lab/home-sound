# Home Sound

Provides speakers as a service arround the hQ home.

This has taken heavy inspiration from [balenaLabs balena-sound project](https://github.com/balenalabs/balena-sound) as
this service was previously provided to the hQ home through [balenaCloud](https://www.balena.io/cloud/) but now with
Nomad being deployed within hQ it makes more sense to manage all services inside hQ inside of Nomad.


