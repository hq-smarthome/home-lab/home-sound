job "home-sound" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  [[ range $job := $.speakers ]]
  group "[[ $job ]]" {
    count = 1

    network {
      port "pulseaudio-tcp" {
        to = 4317
      }
    }

    task "audio-core" {
      driver = "docker"

      config {
        image = "[[ $.imageName ]]"

        ports = ["pulseaudio-tcp"]

        volumes = [
          "/var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket"
        ]

        privileged = false
      }

      service {
        name = "audio-core"
        port = "pulseaudio-tcp"

        check {
          name = "PulseAudio - TCP Server"
          port = "pulseaudio-tcp"
          address_mode = "driver"
          type = "tcp"
          interval = "30s"
          timeout = "5s"
          task = "audio-core"
        }
      }
    }
  } 
  [[ end ]]
}
